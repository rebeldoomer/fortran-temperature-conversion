# Fortran Temperature Conversion

How This Program Works:

Variable Declaration: The program begins by declaring necessary variables. temp_in and temp_out store the input and output temperatures as real numbers. unit_in and unit_out store the single-character unit of temperature (C or F) for input and output, respectively.

User Input: It prompts the user to input a temperature and its unit. The read *, temp_in, unit_in statement reads the temperature and its unit from the standard input.

Temperature Conversion: The program checks the unit of the input temperature. If the unit is Celsius ('C' or 'c'), it converts the temperature to Fahrenheit using the formula (C * 9/5) + 32. If the unit is Fahrenheit ('F' or 'f'), it converts the temperature to Celsius using the formula (F - 32) * 5/9. The conversion result is stored in temp_out, and the output unit is set accordingly in unit_out.

Output: The converted temperature and its unit are printed to the standard output.

Invalid Input Handling: If the input unit is not recognized (not 'C', 'c', 'F', or 'f'), the program prints an error message and exits.

How to Compile and Run:

Assuming you have a Fortran compiler like `gfortran` installed, use the following commands in the terminal to compile and run the program:

    gfortran -o TemperatureConversion Temperature.f90

Then run the executable:
    ./TemperatureConversion

This program is a straightforward example of how to perform basic I/O and calculations in Fortran. I might extend it by adding options for more temperature units (like Kelvin) or improving its user interface in the future.

## example
    user@T460 ~/D/P/Fortran [2]> ./TemperatureConversion
    Enter temperature followed by its unit (C or F):
    40
    c
    Converted temperature:    104.000000  
